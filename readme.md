# Job Application Code Example

## Game API

In this repository you will find a code example meant for current job applications,  
it is based on the Laravel Framework (ver. 5.2)

The code is a Game API website with emphasis on Single Responsibility and Code Readability.  
It is currently hosting 5 games:

- HangMan
- TicTacToe
- Connect 4
- 20 Questions
- MineSweeper

As the original challenge was quite fun to do, I will improve the foundation a bit more to allow for more flexibility
and see how different the games can become.

All games are multiplayer games using WebSockets  
(currently using the external Pusher service, but it is easy to switch over to NodeJS / socket.IO)

Coding was done by TDD and is backed by Unit, Integration and Behavioral tests in phpunit.

## Techniques / Technologies used

- Gulp / Elixir
- Vue JS / Vueify
- Pusher
- PHP: Interfaces / Traits / Abstract Classes / Exceptions / Dependency Injection


## Important locations
- app/Games/*.php : All game classes as popo's
- app/Models/Game.php : The ORM model that saves any game to the DB for resuming.
- app/Events/GameStatus.php : The event broadcaster that sends the status to other players via Pusher
- app/Http/Controllers/GameController.php : The link between any game class and the HTTP world
- resources/assets/js/games/*.vue : The Vue JS Frontend module for each game  
- tests/unit/*.php : Unit tests for each game
- tests/integration/GameTest.php : Integration test for the Game Model <---> Game classes
- tests/behavior/GamePageTest.php : Behavioral test to make sure that HTTP requests respond as expected.



## Installation

- Download the repository
- use "composer install" to install the necessary dependencies
- rename the file ".env.example" to ".env"
- It is setup to use the included sqlite databases, but you can change your storage at this point (in .env)
- For easing the multiplayer part, I have preset the .env.example file with a working pusher account
- To migrate and seed the database: "php artisan migrate:refresh --seed"
- To start the app: "php artisan serve" (or using an external webserver)
- Open the game website on http://localhost:8000
