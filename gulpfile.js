var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

elixir(function(mix) {

    // use "gulp --production" to also minify css and js

    // first, create app.css from the sass file
    // then merge it's outcome with font-awesome and our own css
    // write back to app.css
    mix.sass('app.scss')
        .styles([
            'public/css/app.css',
            'resources/assets/css/font-awesome.min.css',
            'resources/assets/css/main.css'
        ], 'public/css/app.css', './');

    // run our assets/js/app.js through browserify and vueify
    mix.browserify('app.js');

    // and merge all scripts to app.js
    mix.scripts([
        'resources/assets/js/plugins/jquery.min.js',
        'resources/assets/js/plugins/bootstrap.min.js',
        'resources/assets/js/plugins/pusher.min.js',
        'resources/assets/js/plugins/vue.js',
        'public/js/app.js'
    ], 'public/js/app.js', './');
});
