<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {

            $table->increments('id');
            $table->string('type');
            $table->unsignedInteger('creator_id');
            $table->enum('status', ['busy','fail','success']);
            $table->text('data');
            $table->timestamps();
        });

        Schema::create('game_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id');
            $table->unsignedInteger('user_id');
        });
    }

    public function down()
    {
        Schema::drop('games');
        Schema::drop('game_user');
    }
}
