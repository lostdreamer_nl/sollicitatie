<?php

use App\Models\User;

class GamesPageTest extends TestCase
{

    private $type;
    private $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->type = 'HangMan';
        \Auth::login($this->user);
    }

    /** @test */
    public function the_index_should_show_a_list_of_games()
    {
        $this->visit('/games')
                ->see( $this->type );
    }

    /** @test */
    public function you_can_start_a_new_game()
    {
        $this->post('/games', ['type' => $this->type ])
            ->see( $this->type )
            ->see( $this->user->name );
    }

    /** @test */
    public function you_can_see_a_list_of_running_games()
    {
        $this->post('/games', ['type' => $this->type ]);

        $this->visit('/games/'. $this->type )
            ->see( $this->type )
            ->see( $this->user->name );
    }

    /** @test */
    public function other_people_can_join_a_running_game()
    {
        $this->post('/games', ['type' => $this->type]);
        $this->post('/games/1', ['character' => 'a'])
            ->seeJson(["characters" => ["a"]]);

        \Auth::logout();
        $newUser = factory(User::class)->create();
        \Auth::login($newUser);

        $this->post('/games/1/join')
            ->seeJson(["characters" => ["a"]])
            ->seeJson(["email" => $this->user->email])
            ->seeJson(["email" => $newUser->email]);
    }

    /** @test */
    public function you_can_act_in_a_game_by_post_data()
    {
        $this->post('/games', ['type' => $this->type ]);

        $this->post('/games/1', ['character' => 'a'])
            ->seeJson(["characters" => ["a"]]);

        $this->post('/games/1', ['character' => 'b'])
            ->seeJson(["characters" => ["a","b"]]);
    }

    /** @test */
    public function people_can_leave_a_game()
    {
        $this->post('/games', ['type' => $this->type]);
        $this->post('/games/1', ['character' => 'a'])
            ->seeJson(["characters" => ["a"]]);

        \Auth::logout();
        $newUser = factory(User::class)->create();
        \Auth::login($newUser);

        $this->post('/games/1/join')
            ->seeJson(["characters" => ["a"]])
            ->seeJson(["email" => $this->user->email])
            ->seeJson(["email" => $newUser->email]);

        $this->post('/games/1/leave')
            ->seeJson(["email" => $this->user->email])
            ->dontSeeJson(["email" => $newUser->email]);
    }
}
