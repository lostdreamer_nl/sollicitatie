<?php

use App\Exceptions\UnauthorizedException;
use App\Exceptions\InvalidArgumentException;
use App\Models\Game;
use App\Models\User;

/**
 * Game Integration Test
 * These tests will make sure that any game class can be played via the Game Model
 *
 * It should:
 * - be able to start a new game
 * - save it's state to the selected storage engine
 * - check that a user is authenticated
 * - be able to get a running game from the storage engine en resume it
 * - broadcast the current status to all players of the game
 *
 */

class GameTest extends TestCase
{
    private $types;
    private $user;

    public function setUp()
    {
        parent::setUp();

        $this->types = Game::types();

        $this->user = factory(User::class)->create();
        \Auth::login($this->user);
    }

    /** @test */
    public function you_can_only_create_a_game_if_you_are_logged_in()
    {
        \Auth::logout();

        $this->setExpectedException(UnauthorizedException::class);
        Game::create([
            'type' => $this->types->first()
        ]);
    }

    /** @test */
    public function you_can_only_create_a_game_specified_in_the_model()
    {
        $types = Game::types();
        Game::create([
            'type' => $types->first()
        ]);

        $this->setExpectedException(InvalidArgumentException::class);
        Game::create([
            'type' => 'Doesnt Exist'
        ]);
    }

    /** @test */
    public function acting_in_a_game_will_auto_save_and_you_can_resume_from_storage()
    {
        $game = Game::create(['type' => 'HangMan']);
        $game->act(['character' => 'a']);
        $status = $game->status();

        $game = Game::find($game->id);
        $this->assertEquals($status, $game->status());

        $game->act(['character' => 'b']);

        $status = Game::find($game->id)->status();
        $this->assertEquals('a', $status['characters'][0]);
        $this->assertEquals('b', $status['characters'][1]);
    }

}
