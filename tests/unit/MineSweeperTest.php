<?php

use App\Exceptions\GameOverException;
use App\Exceptions\UnauthorizedException;
use App\Games\MineSweeper;
use App\Models\User;

class MineSweeperTest extends TestCase
{
    private $game;
    private $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->be($this->user);
        $this->game = App::make(MineSweeper::class);
    }

    /** @test */
    public function it_starts_with_a_10_x_10_map()
    {
        $this->game->start();
        $this->game->join($this->user);
        $map = $this->game->getMap();
        $this->assertEquals(10, COUNT($map));
        $this->assertEquals(10, COUNT(reset($map)));
    }

    /** @test */
    public function you_can_set_a_different_size_map_at_the_start()
    {
        $this->game->start(5,5);
        $this->game->join($this->user);
        $map = $this->game->getMap();
        $this->assertEquals(5, COUNT($map));
        $this->assertEquals(5, COUNT(reset($map)));
    }

    /** @test */
    public function you_can_set_a_custom_map()
    {
        $this->game->start(5,5);
        $this->game->join($this->user);

        $map = $this->createMap(6,6);

        $this->game->setMap($map);
        $map = $this->game->getMap();

        $this->assertEquals(6, COUNT($map));
        $this->assertEquals(6, COUNT(reset($map)));
    }

    /** @test */
    public function you_can_check_fields_for_mines()
    {
        $this->game->start();
        $this->game->join($this->user);
        $this->game->check(0, 0);
    }

    /** @test */
    public function clicking_a_mine_ends_the_game()
    {
        $this->game->start();
        $this->game->join($this->user);
        $map = $this->createMap(6,6);
        $this->setMine($map, 0, 0);
        $this->game->setMap($map);

        $this->game->check(0, 0);

        $status = $this->game->status();
        $this->assertEquals('fail', $status['status']);
    }

    /** @test */
    public function clicking_a_safe_spot_shows_the_amount_of_bombs_surrounding_it()
    {
        $this->game->start();
        $this->game->join($this->user);
        $map = $this->createMap(6,6);
        $this->setMine($map, 0, 0);
        $this->setMine($map, 1, 0);
        $this->setMine($map, 1, 1);
        $this->game->setMap($map);

        $this->game->check(0, 1);

        $status = $this->game->status();
        $this->assertEquals(3, $status['map'][0][1]);
    }

    /** @test */
    public function clicking_a_spot_with_no_surrounding_mines_clears_surrounding_safe_fields()
    {
        $this->game->start();
        $this->game->join($this->user);
        $map = $this->createMap(6,6);
        $this->setMine($map, 0, 0);
        $this->game->setMap($map);

        $this->game->check(2, 2);

        $status = $this->game->status();
        $this->assertEquals(1, $status['map'][0][1]);
        $this->assertEquals(1, $status['map'][1][0]);
        $this->assertEquals(1, $status['map'][1][1]);
    }

    /** @test */
    public function checking_all_safe_fields_wins_the_game()
    {
        $this->game->start();
        $this->game->join($this->user);
        $map = $this->createMap(6,6);
        $this->setMine($map, 0, 0);
        $this->game->setMap($map);

        $this->game->check(2, 2);

        $status = $this->game->status();
        $this->assertEquals('success', $status['status']);
    }

    protected function createMap($x, $y)
    {
        $map = array_flip(range(1, $x * $y));
        foreach ($map as $i => $field) {
            $map[$i] = null;
        }
        return array_chunk($map, $x);
    }

    protected function setMine(&$map, $x, $y)
    {
        $map[$y][$x] = true;
    }
}
