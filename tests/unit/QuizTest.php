<?php

use App\Exceptions\GameOverException;
use App\Exceptions\UnauthorizedException;
use App\Games\Quiz;
use App\Models\User;

class QuizTest extends TestCase
{
    private $game;
    private $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->be($this->user);
        $this->game = App::make(Quiz::class);
        $this->game->start();
    }

    /** @test */
    public function it_gets_a_random_word_when_starting_the_game()
    {
        $this->assertTrue(strlen($this->game->getWord()) > 1);
    }

    /** @test */
    public function the_first_user_to_join_becomes_the_quiz_master()
    {
        $this->game->join($this->user);
        $user = array_only($this->user->toArray(), ['id', 'name', 'email']);

        $this->assertEquals($user, $this->game->getQuizMaster());
    }

    /** @test */
    public function other_users_can_ask_questions_to_the_quiz_master()
    {
        $user2 = factory(User::class)->create();
        $user3 = factory(User::class)->create();
        $this->joinGame($this->user, $user2, $user3);

        $this->act($user2, ['question' => 'Is het een persoon?']);
    }

    /** @test */
    public function after_a_question_the_quizmaster_can_answer_yes_or_no()
    {
        $user2 = factory(User::class)->create();
        $user3 = factory(User::class)->create();
        $this->joinGame($this->user, $user2, $user3);

        $this->act($user2, ['question' => 'Is het een persoon?']);
        $this->act($this->user, ['answer' => true]);
    }

    /** @test */
    public function asking_multiple_questions_before_getting_an_answer_throws_an_exception()
    {
        $user2 = factory(User::class)->create();
        $user3 = factory(User::class)->create();
        $this->joinGame($this->user, $user2, $user3);

        $this->act($user2, ['question' => 'Is het een persoon?']);

        $this->setExpectedException(UnauthorizedException::class);
        $this->act($user3, ['question' => 'Is het een man?']);
    }

    /** @test */
    public function after_answering_a_question_the_next_player_can_question_the_quizmaster()
    {

        $user2 = factory(User::class)->create();
        $user3 = factory(User::class)->create();
        $user4 = factory(User::class)->create();
        $this->joinGame($this->user, $user2, $user3, $user4);

        $this->act($user2, ['question' => 'Is het een persoon?']);
        $this->act($this->user, ['answer' => true]);
        $this->act($user3, ['question' => 'Is het een man?']);
        $this->act($this->user, ['answer' => true]);
        $this->act($user4, ['question' => 'Is het een vrouw?']);
        $this->act($this->user, ['answer' => false]);

        // $user2 should now be next...
        $this->setExpectedException(UnauthorizedException::class);
        $this->act($user3, ['question' => 'Leeft hij nog?']);
    }

    /** @test */
    public function questions_and_their_answers_are_saved()
    {
        $user2 = factory(User::class)->create();
        $this->joinGame($this->user, $user2);


        $this->act($user2, ['question' => 'Is het een persoon?']);
        $this->act($this->user, ['answer' => true]);
        $this->act($user2, ['question' => 'Is het een man?']);
        $this->act($this->user, ['answer' => false]);

        $status = $this->game->status();
        $this->assertEquals('Is het een persoon?', $status['questions'][0]['question']);
        $this->assertEquals(true, $status['questions'][0]['answer']);
        $this->assertEquals('Is het een man?', $status['questions'][1]['question']);
        $this->assertEquals(false, $status['questions'][1]['answer']);
    }

    /** @test */
    public function you_can_only_ask_20_questions()
    {
        $user2 = factory(User::class)->create();
        $this->joinGame($this->user, $user2);

        foreach(range(1,20) as $i) {
            $this->act($user2, ['question' => 'is it '. $i]);
            $this->act($this->user, ['answer' => false]);
        }
        $this->setExpectedException(GameOverException::class);
        $this->act($user2, ['question' => 'Can i ask 1 more question?']);
    }

    /** @test */
    public function if_the_quiz_master_leaves_the_game_fails()
    {
        $user2 = factory(User::class)->create();
        $this->joinGame($this->user, $user2);
        $status = $this->game->leave($this->user);

        $this->assertEquals('fail', $status['status']);
    }

    private function joinGame(...$users) {
        foreach($users as $user) {
            $this->game->join($user);
        }
    }

    private function act($user, $data) {
        $this->be($user);
        return $this->game->action($data);
    }
}
