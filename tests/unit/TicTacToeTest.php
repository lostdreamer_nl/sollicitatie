<?php

use App\Exceptions\UnauthorizedException;
use App\Games\TicTacToe;
use App\Models\User;

class TicTacToeTest extends TestCase
{
    private $game;
    private $user1;
    private $user2;

    public function setUp()
    {
        parent::setUp();
        $this->game = App::make(TicTacToe::class);
        $this->user1 = factory(User::class)->create();
        $this->user2 = factory(User::class)->create();
        $this->be($this->user1);
    }

    /** @test */
    public function it_starts_with_an_empty_3_by_3_map()
    {
        $status = $this->game->start();

        $this->assertEquals($status['status'], 'busy');
        $this->assertEquals($status['fields'], [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ]);
    }

    /** @test */
    public function users_can_join_a_game_once()
    {
        $this->game->start();
        $this->game->join($this->user1);

        $this->setExpectedException(UnauthorizedException::class);
        $this->game->join($this->user1);
    }

    /** @test */
    public function you_cannot_pick_a_field_without_joining_the_game()
    {
        $this->game->start();

        $this->setExpectedException(UnauthorizedException::class);
        $this->pickField($this->user1, 0, 0);
    }

    /** @test */
    public function you_can_play_the_game_after_the_minimum_players_have_joined()
    {
        $this->game->start();

        $this->game->join($this->user1);
        $this->setExpectedException(UnauthorizedException::class);
        $this->pickField($this->user1, 0, 0);
    }

    /** @test */
    public function only_2_players_can_join_a_game()
    {
        $user3 = factory(User::class)->create();
        $this->game->start();

        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->setExpectedException(UnauthorizedException::class);
        $this->game->join($user3);
    }

    /** @test */
    public function you_cannot_pick_2_fields_in_your_turn()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->pickField($this->user1, 0, 0);
        $this->setExpectedException(UnauthorizedException::class);
        $this->pickField($this->user1, 0, 1);

        $this->pickField($this->user2, 0, 2);

        $status = $this->game->status();
        $this->assertEquals($status['status'], 'busy');
        $this->assertEquals($status['fields'], [
            [1, 0, 2],
            [0, 0, 0],
            [0, 0, 0],
        ]);
    }

    /** @test */
    public function getting_3_in_a_row_wins_the_game()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->pickField($this->user1, 0, 0);
        $this->pickField($this->user2, 1, 0);
        $this->pickField($this->user1, 0, 1);
        $this->pickField($this->user2, 1, 1);
        $this->pickField($this->user1, 0, 2);

        $status = $this->game->status();
        $this->assertEquals('success', $status['status']);
        $this->assertEquals($this->user1->id, $status['winner']['id']);
        $this->assertEquals([
            [1, 1, 1],
            [2, 2, 0],
            [0, 0, 0],
        ], $status['fields']);
    }

    /** @test */
    public function getting_3_in_one_column_wins_the_game()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->pickField($this->user1, 0, 1);
        $this->pickField($this->user2, 0, 0);
        $this->pickField($this->user1, 1, 1);
        $this->pickField($this->user2, 2, 2);
        $this->pickField($this->user1, 2, 1);

        $status = $this->game->status();
        $this->assertEquals('success', $status['status']);
        $this->assertEquals($this->user1->id, $status['winner']['id']);
        $this->assertEquals([
            [2, 1, 0],
            [0, 1, 0],
            [0, 1, 2],
        ], $status['fields']);
    }

    /** @test */
    public function getting_a_full_diagonal_wins_the_game()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->pickField($this->user1, 0, 0);
        $this->pickField($this->user2, 0, 1);
        $this->pickField($this->user1, 1, 1);
        $this->pickField($this->user2, 2, 0);
        $this->pickField($this->user1, 2, 2);

        $status = $this->game->status();
        $this->assertEquals('success', $status['status']);
        $this->assertEquals($this->user1->id, $status['winner']['id']);
        $this->assertEquals([
            [1, 2, 0],
            [0, 1, 0],
            [2, 0, 1],
        ], $status['fields']);
    }

    private function pickField($user, $row, $col)
    {
        $this->be($user);
        return $this->game->action(['row' => $row, 'column' => $col]);
    }
}
