<?php

use App\Exceptions\UnauthorizedException;
use App\Games\Connect4;
use App\Models\User;

class Connect4Test extends TestCase
{
    private $game;
    private $user1;
    private $user2;

    public function setUp()
    {
        parent::setUp();
        $this->game = App::make(Connect4::class);
        $this->user1 = factory(User::class)->create();
        $this->user2 = factory(User::class)->create();
        $this->be($this->user1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->game, $this->user1, $this->user2);
    }

    /** @test */
    public function it_starts_with_an_empty_10_by_10_map()
    {
        $status = $this->game->start();
        $this->assertEquals($status['status'], 'busy');
        $this->assertEquals(count($status['fields']), 10);
        $this->assertEquals(count($status['fields'][0]), 10);
    }

    /** @test */
    public function users_can_join_a_game_once()
    {
        $this->game->start();
        $this->game->join($this->user1);

        $this->setExpectedException(UnauthorizedException::class);
        $this->game->join($this->user1);
    }

    /** @test */
    public function you_cannot_pick_a_field_without_joining_the_game()
    {
        $this->game->start();

        $this->setExpectedException(UnauthorizedException::class);
        $this->pickField($this->user1, 0);
    }

    /** @test */
    public function you_can_play_the_game_after_the_minimum_players_have_joined()
    {
        $this->game->start();

        $this->game->join($this->user1);
        $this->setExpectedException(UnauthorizedException::class);
        $this->pickField($this->user1, 0);
    }

    /** @test */
    public function only_2_players_can_join_a_game()
    {
        $user3 = factory(User::class)->create();
        $this->game->start();

        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->setExpectedException(UnauthorizedException::class);
        $this->game->join($user3);
    }

    /** @test */
    public function you_cannot_pick_2_fields_in_your_turn()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->pickField($this->user1, 0);
        $this->setExpectedException(UnauthorizedException::class);
        $this->pickField($this->user1, 1);
    }

    /** @test */
    public function getting_4_in_a_row_wins_the_game()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->pickField($this->user1, 0);
        $this->pickField($this->user2, 0);
        $this->pickField($this->user1, 1);
        $this->pickField($this->user2, 1);
        $this->pickField($this->user1, 2);
        $this->pickField($this->user2, 2);
        $this->pickField($this->user1, 3);
        $status = $this->game->status();
        $this->assertEquals('success', $status['status']);
        $this->assertEquals($this->user1->id, $status['winner']['id']);
    }

    /** @test */
    public function getting_4_in_one_column_wins_the_game()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);
        
        $this->pickField($this->user1, 1);
        $this->pickField($this->user2, 0);
        $this->pickField($this->user1, 1);
        $this->pickField($this->user2, 2);
        $this->pickField($this->user1, 1);
        $this->pickField($this->user2, 3);
        $this->pickField($this->user1, 1);
    
    
        $status = $this->game->status();
        $this->assertEquals('success', $status['status']);
        $this->assertEquals($this->user1->id, $status['winner']['id']);
    }

    /** @test */
    public function getting_a_full_diagonal_wins_the_game()
    {
        $this->game->start();
        $this->game->join($this->user1);
        $this->game->join($this->user2);

        $this->pickField($this->user1, 0);
        $this->pickField($this->user2, 1);
        $this->pickField($this->user1, 1);
        $this->pickField($this->user2, 2);
        $this->pickField($this->user1, 3);
        $this->pickField($this->user2, 2);
        $this->pickField($this->user1, 2);
        $this->pickField($this->user2, 3);
        $this->pickField($this->user1, 2);
        $this->pickField($this->user2, 3);
        $this->pickField($this->user1, 3);

        $status = $this->game->status();
        $this->assertEquals('success', $status['status']);
        $this->assertEquals($this->user1->id, $status['winner']['id']);
    }

    private function pickField($user, $col)
    {
        $this->be($user);
        return $this->game->action(['column' => $col]);
    }
}
