<?php
use App\Exceptions\GameOverException;
use App\Exceptions\InvalidArgumentException;
use App\Games\HangMan;

/**
 * HANGMAN GAME
 *
 * - The game should start with a random word
 * - That word should be masked
 * - Player(s) have 11 wrong guesses before the game ends
 * - It should treat the word and any character as lowercase
 * - When guessing a character correct it should be unmasked
 * - Unmasking all characters should win the game
 * - Guessing a correct character should not cost a turn
 *
 *
 * EXCEPTIONS
 *
 * - On invalid characters
 * - When guessing on a finnished game
 * - When guessing an already used character
 * - If the player is not logged in
 */
class HangManTest extends TestCase
{
    private $game;

    public function setUp()
    {
        parent::setUp();
        $this->game = App::make(HangMan::class);
    }

    /** @test */
    public function it_gets_a_random_word_when_starting_the_game()
    {
        $this->game->start();
        $this->assertAttributeInternalType('string', 'word', $this->game);
        $this->assertTrue(strlen($this->game->getWord()) > 1);
    }

    /** @test */
    public function it_can_start_a_game_with_a_preset_word()
    {
        $this->game->start('test');
        $this->assertEquals('test', $this->game->getWord(false));
    }

    /** @test */
    public function all_games_have_a_status()
    {
        $status = $this->game->start();

        $this->assertArrayHasKey('word', $status);
        $this->assertArrayHasKey('tries_left', $status);
        $this->assertArrayHasKey('status', $status);
    }

    /** @test */
    public function the_word_should_be_masked()
    {
        $status = $this->game->start();
        $hiddenWord = str_repeat('.', strlen($status['word']));

        $this->assertEquals($hiddenWord, $status['word']);
    }

    /** @test */
    public function a_game_starts_with_11_turns()
    {
        $status = $this->game->start('test');
        $this->assertEquals(11, $status['tries_left']);
    }

    /** @test */
    public function when_guessing_a_character_correct_it_should_unmask_the_character()
    {
        $this->game->start('test');
        $status = $this->game->guess('t');

        $this->assertEquals('t..t', $status['word']);
    }

    /** @test */
    public function it_treats_upper_and_lowercase_letters_the_same()
    {
        $this->game->start('test');
        $status = $this->game->guess('T');

        $this->assertEquals('t..t', $status['word']);
    }

    /** @test */
    public function a_correct_character_does_not_cost_a_turn_while_an_incorrect_one_does()
    {
        $this->game->start('test');
        $status = $this->game->guess('e');

        $this->assertEquals(11, $status['tries_left']);

        $status = $this->game->guess('a');
        $this->assertEquals(10, $status['tries_left']);
    }

    /** @test */
    public function guessing_all_characters_wins_the_game()
    {
        $this->game->start('test');

        $this->game->guess('t');
        $this->game->guess('e');
        $status = $this->game->guess('s');

        $this->assertEquals('test', $status['word']);
        $this->assertEquals('success', $status['status']);
    }

    /** @test */
    public function it_throws_an_exception_when_guessing_non_alphabetic_characters()
    {
        $this->game->start();

        $invalidCharacters = [1,':','.','#'];
        foreach($invalidCharacters as $character) {
            $this->setExpectedException(InvalidArgumentException::class);
            $this->game->guess($character);
        }
    }

    /** @test */
    public function it_throws_an_exception_when_guessing_more_than_1_character()
    {
        $this->game->start();

        $this->setExpectedException(InvalidArgumentException::class);
        $this->game->guess('ab');
    }

    /** @test */
    public function it_throws_an_exception_when_guessing_the_same_character_twice()
    {
        $this->game->start('test');
        $this->game->guess('e');

        $this->setExpectedException(InvalidArgumentException::class);
        $this->game->guess('e');
    }

    /** @test */
    public function it_throws_an_exception_when_guessing_on_a_failed_game()
    {
        $this->game->start('zzzz');

        foreach(range('a', 'k') as $character) {
            $this->game->guess($character);
        }

        $this->setExpectedException(GameOverException::class);
        $this->game->guess('l');
    }


    /** @test */
    public function it_throws_an_exception_when_guessing_on_a_won_game()
    {
        $this->game->start('zzzz');

        $this->game->guess('z');

        $this->setExpectedException(GameOverException::class);
        $this->game->guess('g');
    }
}
