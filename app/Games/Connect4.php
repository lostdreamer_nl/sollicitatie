<?php
namespace App\Games;

use App\Exceptions\InvalidArgumentException;
use App\Models\User;

Class Connect4 extends TicTacToe
{
    protected $fields;
    private $winner = false;

    /**
     * Start a game with an empty 10x10 field
     * @return array $status
     */
    public function start()
    {
        $this->fields = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
        return $this->status();
    }

    /**
     * The bridge between the public action()
     * and this game's pickField() method
     * @param User $user
     * @param array $parameters
     * @return array $status
     * @throws InvalidArgumentException
     */
    protected function act(User $user, array $parameters)
    {
        return $this->pickField($user, $parameters['column']);
    }

    /**
     * Add the game field to
     * the game's status array
     * @return array $status
     */
    public function status()
    {
        $status = parent::status();
        $fields = $this->fields;
        return array_merge($status, compact('fields'));
    }


    /**
     * Determine the winner, if any
     * @return bool
     */
    protected function getWinner()
    {
        return $this->winner;
    }

    protected function gameHasFailed()
    {
        return $this->curTurn == $this->countFields();
    }

    private function pickField(User $user, $col)
    {
        $row = $this->getField($col);
        if ($row === false) {
            throw new InvalidArgumentException('This column is already full');
        }
        $this->setField($user, $row, $col);
        $this->checkForWinner($user, $row, $col);
        return $this->status();
    }

    private function getField($col)
    {
        if (!isSet($this->fields[0][$col])) {
            throw new InvalidArgumentException('column '. $col ." does not exist");
        }
        // check if there is an empty field within this column, starting with the bottom
        $rows = count($this->fields);
        for($row=$rows-1; $row>=0; $row--) {
            if($this->fields[$row][$col] === 0) {
                return $row;
            }
        }
        return false;
    }

    private function checkForWinner($user, $row, $col)
    {
        $sections = [];
        $sections['horizontal'] = $this->getHorizontal($row, $col);
        $sections['vertical'] = $this->getVertical($row, $col);
        $sections['dia1'] = $this->getDiagonal1($row, $col);
        $sections['dia2'] = $this->getDiagonal2($row, $col);
        foreach($sections as $type => $section) {
            $winner = $this->getBestFromSection($section);
            if($winner) {
                $this->winner = $user;
                return;
            }
        }
    }
    
    /**
     * Get an array of numbers, break it up when the value changes
     * then check all groups for the size, if 4, we have a winner
     * @param $section
     * @return bool
     */
    private function getBestFromSection($section)
    {
        $old = null;
        $arrays = $array = [];
        foreach($section as $value) {
            if(is_null($old) || $old == $value) {
                $array[] = $value;
            } else {
                $arrays[] = $array;
                $array = [$value];
            }
            $old = $value;
        }
        if(!empty($array)) {
            $arrays[] = $array;
        }
        $arrays = array_sort($arrays, function($arr) { return count($arr) * -1; });
        $best = reset($arrays);
        if(count($best) == 4) {
            return $best[0];
        }
        return false;
    }
    
    private function getHorizontal($row, $col) {
        $start = max(0, $col - 3);
        $end = min(count($this->fields[$row])-1, $col + 3);
        return array_slice($this->fields[$row], $start, ($end - $start) + 1);
    }
    private function getVertical($row, $col) {
        $start = max(0, $row - 3);
        $end = min(count($this->fields)-1, $row + 3);
        $curFields = [];
        for($i=$start; $i<=$end;$i++) {
            $curFields[] = $this->fields[$i][$col];
        }
        return $curFields;
    }
    private function getDiagonal1($row, $col) {
        $startRow = max(0, $row - 3);
        $endRow = min(count($this->fields)-1, $row + 3);
        $startCol = max(0, $col - 3);
        // bugfix for when startRow wanted to be below 0
        if($row - 3 < 0) {
            $startCol -= $row - 2;
        }
        $endCol = min(count($this->fields[$row])-1, $col + 3);
        $curFields = [];
        $col = $startCol;
        $row=$startRow;
        while($row <= $endRow && $col <= $endCol) {
            $curFields[] = $this->fields[$row][$col];
            $row++;
            $col++;
        }
        return $curFields;
    }
    private function getDiagonal2($row, $col) {
        $startRow = max(0, $row - 3);
        $endRow = min(count($this->fields)-1, $row + 3);
        $startCol = min(count($this->fields[$row])-1, $col + 3);
        // bugfix for when startRow wanted to be below 0
        if($row - 3 < 0) {
            $startCol += $row - 3;
        }
        $endCol = max(0, $col - 3);
//    dd($startRow, $endRow, $startCol, $endCol);
        $curFields = [];
        $col = $startCol;
        $row=$startRow;
        while($row <= $endRow && $col >= $endCol) {
            $curFields[] = $this->fields[$row][$col];
            $row++;
            $col--;
        }
        return $curFields;
    }

    private function setField(User $user, $row, $col)
    {
        $this->curTurn++;
        $this->fields[$row][$col] = $user->id;
    }

    protected function gameIsOver()
    {
        return $this->gameHasFailed() || $this->getWinner();
    }

    private function countFields()
    {
        return count($this->fields) * count( reset($this->fields) );
    }

}