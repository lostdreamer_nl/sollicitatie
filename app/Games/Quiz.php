<?php
namespace App\Games;

use App\Exceptions\GameOverException;
use App\Exceptions\UnauthorizedException;
use App\Libraries\RandomWord;
use App\Models\User;
use Illuminate\Support\Collection;

Class Quiz extends BaseGame
{
    protected $minUsers = 2;
    protected $maxUsers = 5;
    protected $maxQuestions= 20;
    private $wordRepository;
    private $word;
    private $questions;
    private $quizMaster;
    private $status = 'busy';

    /**
     * Use App:make(Quiz::class)to construct
     * @param Collection $collection
     * @param RandomWord $wordRepository
     */
    public function __construct(Collection $collection, RandomWord $wordRepository)
    {
        parent::__construct($collection);
        $this->wordRepository = $wordRepository;
    }

    /**
     * Start a new quiz with a predefined word
     * or a random auto-generated one
     * @param null $word
     * @return array
     * @throws UnauthorizedException
     */
    public function start($word = null)
    {
        if ($this->word) {
            throw new UnauthorizedException('This game started already');
        }
        # Start with a predefined word, or get a random word
        $this->word = $word ?: $this->wordRepository->get();
        return $this->status();
    }

    /**
     * Overwriting the parent's changeTurn
     * User 2 will start then quizMaster
     * then User 3, quizMaster again
     * ...
     */
    protected function changeTurn()
    {
        if ($this->isQuizMasterTurn()) {
            $this->curUser = $this->lastQuestioner() + 1;
        } else {
            // user 0 = the quizMaster
            $this->curUser = 0;
        }
        // Round robin when we're done
        if ($this->curUser == $this->users->count()) {
            $this->curUser = 1;
        }
    }

    /**
     * Extending the parent's default Join
     * The first one to join will become the quizMaster
     * The second one to join will take the first turn
     * @param User $user
     * @return array $status
     */
    public function join(User $user)
    {
        if (empty($this->getUserList())) {
            $this->quizMaster = $this->transformUser($user);
        } else {
            if (!count($this->questions)) {
                $this->curUser = 1;
            }
        }
        return parent::join($user);
    }

    /**
     * Extending the default leave function
     * When the quizMaster leaves, the game has failed
     * @param User $user
     * @return array $status
     * @throws UnauthorizedException
     */
    public function leave(User $user)
    {
        if($this->quizMaster == $this->transformUser($user)) {
            $this->status = 'fail';
        }
        return parent::leave($user);
    }

    /**
     * Get the status data for this game
     * @return array
     */
    public function status()
    {
        $status = parent::status();
        $word = $this->getWord();
        $questions = $this->questions;
        $quiz_master = $this->getQuizMaster();
        return array_merge($status, compact('quiz_master', 'questions', 'word'));
    }

    public function getWord()
    {
        return $this->word;
    }

    public function getQuizMaster()
    {
        return $this->quizMaster;
    }

    protected function act(User $user, array $params)
    {
        if($this->isQuizMasterTurn()) {
            return $this->quizMasterAct($params);
        }
        return $this->askQuestion($user, $params);
    }

    protected function gameIsOver()
    {
        return $this->status != 'busy';
    }

    protected function gameHasFailed()
    {
        return $this->status == 'fail';
    }

    protected function getWinner()
    {
        if($this->status == 'success') {
            # Grab last person asking question...
            $user = $this->questions[COUNT($this->questions) - 1]['user'];
            return $this->getUserList()[$user];
        }
        return false;
    }

    private function isQuizMasterTurn()
    {
        return $this->curUser == 0;
    }

    /**
     * Get the user ID of the last questioner
     * @return array|mixed
     */
    protected function lastQuestioner()
    {
        $lastQuestion = last($this->questions);
        $key = array_keys($this->users->filter(function ($user) use ($lastQuestion) {
            return $user['id'] == $lastQuestion['user'];
        })->toArray());
        $key = reset($key);
        return $key;
    }

    /**
     * Save the quiz master's answer,
     * possibilities are fail / succes / true / false
     * @param $params
     * @return mixed
     */
    private function quizMasterAct($params)
    {
        if( in_array($params['answer'], ['fail', 'success'], true)) {
            return $this->status = $params['answer'];
        }
        $this->questions[COUNT($this->questions) - 1]['answer'] = $params['answer'];
    }

    private function askQuestion(User $user, $params)
    {
        if(COUNT($this->questions) == $this->maxQuestions) {
            throw new GameOverException();
        }
        $this->questions[] = [
            'question' => $params['question'],
            'user' => $user->id,
            'answer' => ''
        ];
    }
}