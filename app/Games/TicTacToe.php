<?php
namespace App\Games;

use App\Exceptions\UnauthorizedException;
use App\Exceptions\GameOverException;
use App\Exceptions\InvalidArgumentException;
use App\Models\User;

Class TicTacToe extends BaseGame
{
    protected $minUsers = 2;
    protected $maxUsers = 2;
    protected $curTurn = 0;
    private $fields;

    /**
     * Start a game with an empt 3x3 field
     * @return array $status
     */
    public function start()
    {
        $this->fields = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ];
        return $this->status();
    }

    /**
     * The bridge between the public action()
     * and this game's pickField() method
     * @param User $user
     * @param array $parameters
     * @return array $status
     * @throws InvalidArgumentException
     */
    protected function act(User $user, array $parameters)
    {
        return $this->pickField($user, $parameters['row'], $parameters['column']);
    }

    /**
     * Add the game field to
     * the game's status array
     * @return array $status
     */
    public function status()
    {
        $status = parent::status();
        $fields = $this->fields;
        return array_merge($status, compact('fields'));
    }


    /**
     * Determine the winner, if any
     * @return bool
     */
    protected function getWinner()
    {
        // todo: refactor this for upcoming 'connect 4' etc.
        foreach ($this->getUserList() as $id => $name) {
            if ($this->userHasFullRow($id)
                || $this->userHasFullColumn($id)
                || $this->userHasFullDiagonal($id) ) {
                return $this->getUserList()[$id];
            }
        }
        return false;
    }

    protected function gameHasFailed()
    {
        return $this->curTurn == $this->countFields();
    }

    private function pickField(User $user, $row, $col)
    {
        if ($this->getField($row, $col)) {
            throw new InvalidArgumentException('This field has been taken already');
        }
        $this->setField($user, $row, $col);
        return $this->status();
    }

    private function getField($row, $col)
    {
        if (!isSet($this->fields[$row]) || !isSet($this->fields[$row][$col])) {
            throw new InvalidArgumentException($row ."x". $col ." does not exist");
        }
        return $this->fields[$row][$col];
    }

    private function userHasFullRow($userId)
    {
        foreach ($this->fields as $row) {
            if(count(array_unique($row)) === 1
                && reset($row) == $userId) {
                return $userId;
            }
        }
        return false;
    }

    private function userHasFullColumn($userId)
    {
        $cols = reset($this->fields);
        foreach ($cols as $col => $value) {
            if ($this->fields[0][$col] == $userId
                && $this->fields[1][$col] == $userId
                && $this->fields[2][$col] == $userId
            ) {
                return $userId;
            }
        }
        return false;
    }

    private function userHasFullDiagonal($userId)
    {
        if ($this->fields[0][0] == $userId
            && $this->fields[1][1] == $userId
            && $this->fields[2][2] == $userId) {
            return $userId;
        }
        if ($this->fields[0][2] == $userId
            && $this->fields[1][1] == $userId
            && $this->fields[2][0] == $userId) {
            return $userId;
        }
        return false;
    }

    private function setField(User $user, $row, $col)
    {
        $this->curTurn++;
        $this->fields[$row][$col] = $user->id;
    }

    protected function gameIsOver()
    {
        return $this->gameHasFailed() || $this->getWinner();
    }

    private function countFields()
    {
        return count($this->fields) * count( reset($this->fields) );
    }

}