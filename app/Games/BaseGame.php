<?php
namespace App\Games;

use App\Exceptions\GameOverException;
use App\Exceptions\UnauthorizedException;
use App\Models\User;
use Illuminate\Support\Collection;
use App\Contracts\Playable;

/**
 * Any Game has:
 * - a minimum and maximum number of users before playing can begin
 * - an 'act' method for handling user input on each turn
 * - a 'gameIsOver' method which should return true when the game is over
 * - a 'gameHasFailed' method that returns true if the game has ended in faillure
 * - a 'getWinner' method that returns false or the name of the winner

/**
 * Any Game has:
 * - a list of users
 * - a minimum amount of players before playing can begin
 * - a maximum amount of players up to which players can join
 *
 *  The games should implement:
 * - an 'act' method for handling user input on each turn
 * - a 'gameHasFailed' method which should return true when the game has failed
 * - a 'getWinner' method which should return user data of the winner
 *
 * The BaseGame will handle joining of users, and manage whose turn it is.
 * act() will not be called by users who are not joined in the game or before their turn.
 */
abstract class BaseGame implements Playable
{
    protected $minUsers = 1;
    protected $maxUsers = 1;
    protected $users;
    protected $curUser = 0;

    /**
     * The bridge method between the outer world and
     * and the specific game that has been chosen
     * it will only be used by the current user
     * @param User $user
     * @param array $parameters
     */
    protected abstract function act(User $user, array $parameters);

    /**
     * Determine if the game is over
     * @return bool
     */
    protected abstract function gameIsOver();

    /**
     * Determine if the game has failed
     * @return bool
     */
    protected abstract function gameHasFailed();

    /**
     * Get the user that has won the game or return false
     * @return false | user
     */
    protected abstract function getWinner();

    /**
     * Use App:make('FQN\GameType')to construct
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        $this->users = $collection;
    }

    /**
     * Let a user join the game after running the needed checks
     * @param User $user
     * @return array
     * @throws UnauthorizedException
     */
    public function join(User $user)
    {
        $user = $this->transformUser($user);

        $this->runPreJoinChecks($user);

        $this->users->push($user);

        return $this->status();
    }

    /**
     * Leave a game you have joined earlier
     * @param User $user
     * @return array
     * @throws UnauthorizedException
     */
    public function leave(User $user)
    {
        $user = $this->transformUser($user);

        if ( ! $this->userHasJoinedAlready($user)) {
            throw new UnauthorizedException('You cannot leave the game if you havent joined');
        }
        $this->users->forget( $this->users->search($user) );

        return $this->status();
    }

    /**
     * Act in a game, every game depends on it's own type of params
     * @param array $params
     * @return $this
     * @throws GameOverException
     * @throws UnauthorizedException
     */
    public function action(array $params)
    {
        $user = \Auth::user();

        $this->runPreActionChecks($user);

        $this->act($user, $params);

        $this->changeTurn();

        return $this;
    }

    /**
     * Get the status array with minimum keys for any game
     * @return array ['users', 'status', 'winner']
     */
    public function status()
    {
        $users = $this->getUserList();
        $status = 'busy';
        if ($winner = $this->getWinner()) {
            $status = 'success';
        } else if ($this->gameHasFailed()) {
            $status = 'fail';
        }
        return compact('users', 'status', 'winner');
    }

    protected function userIsNext(User $user)
    {
        $user = $this->transformUser($user);
        return $this->users[$this->curUser] == $user;
    }

    protected function changeTurn()
    {
        $this->curUser++;
        if ($this->curUser == $this->users->count()) {
            $this->curUser = 0;
        }
    }

    protected function getUserList()
    {
        return $this->users->keyBy('id')->toArray();
    }

    private function runPreJoinChecks($user)
    {
        if ($this->userHasJoinedAlready($user)) {
            throw new UnauthorizedException('You already joined');
        }
        if ($this->gameIsFull()) {
            throw new UnauthorizedException('Too many users');
        }
    }

    private function runPreActionChecks($user)
    {
        if (!$user) {
            throw new UnauthorizedException('You must be logged in to play games');
        }
        if (!$this->userHasJoinedAlready($user)) {
            throw new UnauthorizedException('You must first join this game');
        }
        if ($this->gameIsOver()) {
            throw new GameOverException();
        }
        if (!$this->userIsNext($user)) {
            throw new UnauthorizedException('It is not your turn');
        }
        if (!$this->hasEnoughUsers()) {
            throw new UnauthorizedException('There are not enough players yet, minimum = ' . $this->minUsers);
        }
    }

    private function userHasJoinedAlready($user)
    {
        return $this->users->where('id', $user['id'])->count() > 0;
    }

    private function gameIsFull()
    {
        return $this->users->count() == $this->maxUsers;
    }

    private function hasEnoughUsers()
    {
        return $this->minUsers <= $this->users->count();
    }

    protected function transformUser(User $user)
    {
        return array_only($user->toArray(), ['id', 'name', 'email']);
    }

}