<?php
namespace App\Games;

use App\Models\User;

class MineSweeper extends BaseGame
{
    protected $maxUsers = 1;
    protected $map;
    protected $status = 'busy';
    protected $winner = false;
    protected $openFields = [];

    public function start($width = 10, $height = 10, $bombs = 10)
    {
        $this->map = $this->createMap($width, $height, $bombs);
    }

    public function getMap()
    {
        return $this->map;
    }

    public function setMap(array $map)
    {
        $this->map = $map;
    }

    public function check($row, $col)
    {
        if(!$this->fieldExists($row, $col)) {
            // todo: throw exception
            return false;
        }
        $this->openFields[] = [$row, $col];
        if($this->fieldHasMine($row, $col)) {
            $this->status = 'fail';
        } else {
            $this->setSurroundingMines($row, $col);
            $this->checkForWinner();
        }
        return $this->map[$row][$col];
    }

    public function status()
    {
        $map = $this->getMap();
        return parent::status() + compact('map');
    }

    /**
     * The bridge method between the outer world and
     * and the specific game that has been chosen
     * it will only be used by the current user
     * @param User $user
     * @param array $parameters
     */
    protected function act(User $user, array $parameters)
    {
        $this->check($parameters['row'], $parameters['column']);
    }

    /**
     * Determine if the game is over
     * @return bool
     */
    protected function gameIsOver()
    {
        return $this->gameHasFailed() || $this->getWinner();
    }

    /**
     * Determine if the game has failed
     * @return bool
     */
    protected function gameHasFailed()
    {
        return $this->status != 'busy';
    }

    /**
     * Get the user that has won the game or return false
     * @return false | user
     */
    protected function getWinner()
    {
        return $this->winner;
    }

    /**
     * @param $width
     * @param $height
     * @param $bombs
     * @return array
     */
    private function createMap($width, $height, $bombs)
    {
        $map = array_flip(range(1, $width * $height));
        foreach ($map as $i => $field) {
            $map[$i] = null;
        }
        for ($i = 1; $i <= $bombs; $i++) {
            $map[$i] = true;
        }
        shuffle($map);
        return array_chunk($map, $width);
    }

    private function fieldHasMine($row, $col)
    {
        if(!$this->fieldExists($row, $col)) {
            return false;
        }
        return $this->map[$row][$col] === true;
    }

    private function setSurroundingMines($row, $col)
    {
        if(!$this->fieldExists($row, $col) || !is_null($this->map[$row][$col])) {
            return false;
        }
        $mines = 0;
        $fields = $this->getSurroundingFields($row, $col);
        foreach($fields as $field) {
            $mines += (int) $this->fieldHasMine($field[0], $field[1]);
        }

        $this->map[$row][$col] = $mines;
        if($mines === 0) {
            $this->clearSurrounding($row, $col);
        }
    }

    private function clearSurrounding($row, $col)
    {
        $fields = $this->getSurroundingFields($row, $col);
        foreach($fields as $field) {
            $this->setSurroundingMines($field[0], $field[1]);
        }
    }

    private function getSurroundingFields($row, $col) {
        $fields = [];
        $fields[] = [$row-1, $col-1];
        $fields[] = [$row,   $col-1];
        $fields[] = [$row+1, $col-1];
        $fields[] = [$row-1, $col];
        $fields[] = [$row+1, $col];
        $fields[] = [$row-1, $col+1];
        $fields[] = [$row,   $col+1];
        $fields[] = [$row+1, $col+1];
        return $fields;
    }

    /**
     * @param $row
     * @param $col
     * @return bool
     */
    private function fieldExists($row, $col)
    {
        return array_key_exists($row, $this->map) && array_key_exists($col, $this->map[$row]);
    }

    private function checkForWinner()
    {
        // check if we have fields with NULL
        foreach($this->map as $row) {
            foreach($row as $col) {
                if(is_null($col)) {
                    return false;
                }
            }
        }
        $this->winner = $this->users[$this->curUser];
        return true;
    }
}