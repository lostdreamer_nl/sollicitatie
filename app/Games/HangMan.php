<?php

namespace App\Games;

use App\Exceptions\GameOverException;
use App\Exceptions\InvalidArgumentException;
use App\Libraries\RandomWord;
use App\Models\User;
use Illuminate\Support\Collection;


class HangMan extends BaseGame
{
    const MASK_CHAR = '.';

    private $wordRepository;
    private $word;
    private $characters = [];
    private $triesLeft = 11;
    protected $maxUsers = 3;

    /**
     * Use App:make(HangMan::class)to construct
     * @param Collection $collection
     * @param RandomWord $wordRepository
     */
    public function __construct(Collection $collection, RandomWord $wordRepository)
    {
        parent::__construct($collection);
        $this->wordRepository = $wordRepository;
    }

    /**
     * Start a game with your predefined word
     * or let the system get a random one
     * either from external or storage
     * @param null $word
     * @return array
     */
    public function start($word = null)
    {
        $this->word = $word ?: $this->wordRepository->get();
        return $this->status();
    }

    /**
     * Get the current word, either masked or not
     * @param bool $masked
     * @return mixed
     */
    public function getWord($masked = true)
    {
        if (!$masked) {
            return $this->word;
        }
        # Replace non guessed characters with masking characters
        $hidden = array_diff($this->getValidCharacters(), $this->characters);
        return str_replace($hidden, self::MASK_CHAR, $this->word);
    }

    /**
     * Get the status data for this game
     * @return array
     */
    public function status()
    {
        $gameStatus = parent::status();
        $tries_left = $this->triesLeft;
        // if $tries_left = 0 the word will be unmasked
        $word = $this->getWord($tries_left);
        $status = $this->getStatus();
        $characters = $this->characters;

        return array_merge($gameStatus, compact('tries_left', 'word', 'status', 'characters'));
    }

    /**
     * The bridge method between the outer world
     * and the HangMan game
     * It's job is easy, guess the next character
     * @param User $user
     * @param array $parameters
     * @return array $status
     * @throws GameOverException | InvalidArgumentException
     */
    public function act(User $user, array $parameters)
    {
        return $this->guess($parameters['character']);
    }

    /**
     * Guess a character,
     * It will run some default checks to see if the player is allowed
     * @param $character
     * @return array
     * @throws GameOverException
     * @throws InvalidArgumentException
     */
    public function guess($character)
    {
        $character = strtolower($character);

        if ($this->gameHasFailed()) {
            throw new GameOverException('This game was lost already');
        }
        if ($this->gameWasWon()) {
            throw new GameOverException('This game was won already');
        }
        if ($this->characterIsInvalid($character)) {
            throw new InvalidArgumentException('only a-z are allowed');
        }
        if ($this->characterIsUsedAlready($character)) {
            throw new InvalidArgumentException($character . ' has already been used');
        }

        $this->addCharacterToList($character);

        $this->setTriesLeft($character);

        return $this->status();
    }

    private function characterIsInvalid($character)
    {
        return array_search($character, $this->getValidCharacters()) === false;
    }

    private function getValidCharacters()
    {
        return range('a', 'z');
    }

    private function gameWasWon()
    {
        return strpos($this->getWord(), self::MASK_CHAR) === false;
    }

    private function characterIsUsedAlready($character)
    {
        return array_search($character, $this->characters) !== false;
    }

    private function setTriesLeft($character)
    {
        if (strpos($this->word, $character) === false) {
            $this->triesLeft--;
        }
    }

    private function addCharacterToList($character)
    {
        array_push($this->characters, $character);
    }

    private function getStatus()
    {
        $status = 'busy';
        if ($this->gameWasWon()) {
            $status = 'success';
            return $status;
        } else if ($this->gameHasFailed()) {
            $status = 'fail';
            return $status;
        }return $status;
    }

    protected function gameIsOver()
    {
        return $this->gameWasWon() || $this->gameHasFailed();
    }

    protected function gameHasFailed()
    {
        return $this->triesLeft === 0;
    }

    protected function getWinner()
    {
        // TODO: Implement getWinner() method.
    }
}
