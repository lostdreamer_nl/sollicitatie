<?php

namespace App\Events;

use App\Models\Game;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Broadcast the game data and the current user
 */
class GameStatus extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $game;
    public $user;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->user = \Auth::user();
    }

    public function broadcastOn()
    {
        return [
            'game_'. $this->game->id
        ];
    }
}
