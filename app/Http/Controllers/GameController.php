<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * List the index of gameTypes or it's games
     * @param null $type
     * @return \Illuminate\Support\Collection
     */
    public function index($type = null)
    {
        if (!$type) {
            return Game::types();
        }
        return Game::whereType($type)
            ->whereStatus('busy')->orderBy('id', 'desc')->get();
    }

    /**
     * Start a new game
     * @param Request $request
     * @return $this|null
     */
    public function start(Request $request)
    {
        return Game::create([
            'type' => $request->type
        ])->fresh();
    }

    /**
     * Join a running game
     * @param $id
     * @return mixed
     */
    public function join($id)
    {
        return Game::findOrFail($id)
            ->join();
    }

    /**
     * Leave a game you have joined earlier
     * @param $id
     * @return mixed
     */
    public function leave($id)
    {
        return Game::findOrFail($id)
            ->leave();
    }

    /**
     * Act in the game
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function act(Request $request, $id)
    {
        $game = Game::findOrFail($id);
        if ($request->isMethod('post')) {
            $game = $game->act($request->all());
        }
        return $game;
    }

}
