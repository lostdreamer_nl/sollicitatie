<?php


Route::group(['middleware' => 'web'], function () {

    Route::auth();

    Route::group(['middleware' => 'auth'], function () {

        // list all games
        Route::get('/games', 'GameController@index');

        // list all running games of type $type
        Route::get('/games/{type}', 'GameController@index');

        // start a new game
        Route::post('/games', 'GameController@start');

        // join game
        Route::post('/games/{id}/join', 'GameController@join');

        // leave game
        Route::post('/games/{id}/leave', 'GameController@leave');

        // act in a game or get it's current status
        Route::any('/games/{id}', 'GameController@act')
            ->where('id', '[0-9]+');

        // index page
        Route::get('/', function () {
            return view('games');
        });
    });

});

