<?php
namespace App\Models;

use App\BaseModel;
use App\Events\GameStatus;
use App\Exceptions\GameException;
use App\Exceptions\InvalidArgumentException;
use App\Exceptions\UnauthorizedException;
use Illuminate\Support\Collection;


class Game extends BaseModel
{
    // These fields need to be fillable
    protected $fillable = ['type', 'creator_id', 'status', 'data'];
    // It will always include its users and creator
    protected $with = ['creator', 'users'];
    // The data field is serialized data, we'd rather not show that
    protected $hidden = ['data'];
    // Instead we'll show the game_status attribute via the getGameStatusAttribute() method
    protected $appends = ['game_status'];
    // This holds the actual game being played
    private $game;

    // List all playable games here
    private static $types = [
        'Connect4',
        'HangMan',
        'MineSweeper',
        'TicTacToe',
        'Quiz',
    ];

    /**
     * A game has a single creator
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    /**
     * A game has many users / players
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Start the selected game
     * @return $this
     */
    public function start()
    {
        $this->game->start();
        $this->save();
        return $this;
    }

    /**
     * Returns the status data for the current game
     * @return array $status
     */
    public function status()
    {
        return $this->game->status();
    }

    /**
     * Let a (or the current) user join a game
     * @param User|null $user
     * @return $this|null
     * @throws GameException
     */
    public function join(User $user = null)
    {
        $user = $user ?: \Auth::user();
        if (!$this->users()->where('user_id', $user->id)->count()) {
            Try {
                $this->game->join(\Auth::user());
                $this->users()->attach($user);
                $this->save();
            } catch (GameException $e) {
                $e->setGame($this);
                throw $e;
            }
        }
        return $this->fresh($this->with);
    }

    /**
     * Let a (or the current) user leave a game he joined
     * @param User|null $user
     * @return $this|null
     * @throws GameException
     */
    public function leave(User $user = null)
    {
        $user = $user ?: \Auth::user();
        if ($this->users()->where('user_id', $user->id)->count()) {
            Try {
                $this->game->leave(\Auth::user());
                $this->users()->detach($user);
                $this->save();
            } catch (GameException $e) {
                $e->setGame($this);
                throw $e;
            }
        }
        return $this->fresh($this->with);
    }

    /**
     * Act in the current game
     * @param array $request
     * @return $this
     * @throws GameException
     */
    public function act(Array $request)
    {
        Try {
            $this->game->action($request);
        } catch (GameException $e) {
            $e->setGame($this);
            throw $e;
        }
        $this->save();
        return $this;
    }


    /**
     * Get all the game types to choose from
     * @return Collection $types
     */
    public static function types()
    {
        return new Collection(self::$types);
    }

    /**
     * Type mutator to make sure the type is valid
     * @param $type
     * @throws InvalidArgumentException
     */
    public function setTypeAttribute($type)
    {
        if (array_search($type, self::$types) === false) {
            throw (new InvalidArgumentException)->setGame($this);
        }
        $this->attributes['type'] = $type;
    }

    /**
     * Extending Eloquent's newFromBuilder
     * When we get a game from storage.
     * We will first unserialize it.
     * @param array $attributes
     * @param null $connect
     * @return static
     */
    public function newFromBuilder($attributes = [], $connect = null)
    {
        $model = parent::newFromBuilder($attributes, $connect = null);
        if ($model->data) {
            $model->game = unserialize($model->data);
        }
        return $model;
    }

    /**
     * Pass through the game's status via the game_status attribute
     * @return array
     */
    public function getGameStatusAttribute()
    {
        return $this->status();
    }


    /**
     * Make sure only logged in users can create games
     * @param $model
     * @return bool
     * @throws UnauthorizedException
     */
    public function beforeCreate($model)
    {
        $userId = \Auth::id();
        if (!$userId) {
            throw (new UnauthorizedException)->setGame($this);
        }
        $model->creator_id = $userId;
        return true;
    }

    /**
     * Make the creator always join the game first
     * @param $model
     */
    public function afterCreate($model)
    {
        $model->join(\Auth::user());
    }

    /**
     * Every time we save the game, we will also save
     * it's current status (busy | fail | success)
     * So we can easily select running games
     * @param $model
     * @return bool
     */
    public function beforeSave($model)
    {
        if (!$model->game) {
            $model->game = \App::make('App\\Games\\' . $model->type);
            $model->game->start();
        }
        $model->status = $model->gameStatus['status'];
        $model->data = serialize($model->game);
        return true;
    }

    /**
     * Every time we changed a game
     * We'll signal other players
     * and send them the status
     * @param $game
     */
    public function afterSave($game)
    {
        event(new GameStatus($game));
    }
}
