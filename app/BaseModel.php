<?php
namespace App;

use App\Traits\SelfListeningTrait;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use SelfListeningTrait;

    protected $guarded = ['id'];

}
