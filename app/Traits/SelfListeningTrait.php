<?php
namespace App\Traits;

/**
 *  Any model can use the following functions to listen to Eloquent Events
 *  before/after (Save|Create|Update|Delete)
 *  ie: beforeUpdate($model)
 *      afterCreate($model)
 */
Trait  SelfListeningTrait
{
    public static function bootSelfListeningTrait() {
        $myself   = get_called_class();
        $hooks    = array('before' => 'ing', 'after' => 'ed');
        $radicals = array('sav', 'creat', 'updat', 'delet');
        foreach ($radicals as $rad) {
            foreach ($hooks as $hook => $event) {
                // beforeSave | beforeCreate | afterSave | afterCreate ...
                $method = $hook.ucfirst($rad).'e';
                if (method_exists($myself, $method)) {
                    // saving | creating | saved | created ...
                    $eventMethod = $rad.$event;
                    self::$eventMethod(function($model) use ($method) {
                        return $model->$method($model);
                    });
                }
            }
        }
    }

}