<?php

namespace App\Exceptions;

use App\Models\Game;

class GameException extends \Exception
{
    private $game;

    public function setGame(Game $game) {
        $this->game = $game;
        return $this;
    }

    public function getGame() {
        return $this->game->toArray();
    }
}