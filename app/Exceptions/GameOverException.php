<?php
namespace App\Exceptions;


class GameOverException extends GameException
{
    protected $message = 'This game is over';
}