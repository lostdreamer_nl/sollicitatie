<?php
namespace App\Contracts;

use App\Models\User;

interface Playable
{

    public function start();

    public function join(User $user);

    public function leave(User $user);

    public function action(array $request);

    public function status();


}