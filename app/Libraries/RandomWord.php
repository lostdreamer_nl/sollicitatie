<?php
namespace App\Libraries;


use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Get a random dutch word from randomwoorden.nl
 * if it fails; fallback to our own storage
 */
class RandomWord
{
    public function get()
    {
        $word = '';
        while( !$this->isValid($word) ) {
            $word = $this->getWord();
        }
        return $word;
    }

    private function getWord()
    {
        Try {
            $client = new Client();
            $word = $client->get('http://randomwoorden.nl');
            $dom = new Crawler($word->getBody()->__toString());
            return strtolower($dom->filter('#woord')->text());
        } catch(\Exception $e) {
            // website unreachable ?
            return $this->randomWordFromDB();
        }
    }

    private function isValid($word)
    {
        return strlen($word) > 5  && ctype_lower($word);
    }

}