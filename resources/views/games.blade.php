@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <p v-if=" !currentGame ">
                    Welkom @{{ user.name }} <br/>
                    Kies een spel om te spelen
                </p>

                <div class="form-group" v-if=" !gameType ">
                    <select v-model="gameType" class="form-control" @change="getActiveGames()">
                        <option value="">Kies een spel</option>
                        <option v-for="game in games" :value="game">@{{ game }}</option>
                    </select>
                </div>

                <div v-else>
                    <a href="#" @click="back()" class="btn btn-default">Terug</a>
                </div>

                <div v-if=" !currentGame && gameType ">
                    <div v-if=" gameType " class="list-group">
                        <a href="#" @click="startGame(gameType)" class="list-group-item">Start een nieuw spel @{{ gameType }}</a>

                        <a href="#" v-for="game in list" @click="joinGame(game)" class="list-group-item clearfix">
                            @{{ game.creator.name }}
                            <div class="badge">@{{ game.users.length }} speler</div>
                        </a>

                    </div>
                </div>

                <div v-if="currentGame">
                    <component :is="currentGame.type.toLowerCase()" :game.sync="currentGame"></component>
                </div>

            </div>
        </div>
    </div>

@endsection

