var Vue = require('vue');

import hangman from './games/HangMan.vue';
import minesweeper from './games/MineSweeper.vue';
import connect4 from './games/Connect4.vue';
import tictactoe from './games/TicTacToe.vue';
import quiz from './games/Quiz.vue';


Vue.config.debug = true;

// more of a POC then anything else....
new Vue({
    el: 'body',
    data: {
        user: user,
        games: [],
        gameType: null,
        currentGame: null,
        userList: user,
        list: []
    },
    components: {
        hangman,
        connect4,
        minesweeper,
        tictactoe,
        quiz
    },
    ready: function(){
        this.getGameList();
    },
    watch: {
        gameType: function(newValue) {
            if(newValue) {
                this.getActiveGames();
            }
        }
    },
    methods: {
        back: function() {
            var self = this;
            if(this.currentGame) {
                $.post('games/' + this.currentGame.id + '/leave', {_token: token}, function(data){
                    pusher.unsubscribe('game_'+ self.currentGame.id);
                    self.currentGame = null;
                    self.getActiveGames();
                }, 'json');
                return;
            }
            this.gameType = null;
            this.list = []
        },
        getGameList: function() {
            var self = this;
            $.get('games', function(data){
                self.$set('games', data);
            }, 'json');
        },
        getActiveGames: function() {
            var self = this;
            $.get('games/' + this.gameType, function(data){
                self.$set('list', data);
            }, 'json');
        },
        joinGame: function(game) {
            var self = this;
            if(self.currentGame) {
                $.post('games/' + game.id + '/leave', {_token: token}, function(data){
                    pusher.unsubscribe('game_'+ self.currentGame.id);
                }, 'json');
            }
            $.post('games/' + game.id + '/join', {_token: token}, function(game){
                self.$set('currentGame', game);
                channel = pusher.subscribe('game_'+ game.id);
                channel.bind('App\\Events\\GameStatus', function(data) {
                    self.$set('currentGame', data.game);
                });
            }, 'json');
        },
        startGame: function(game) {
            var self = this;
            $.post('games', {_token: token, type: game}, function(data){
                self.joinGame(data);
            }, 'json');
        },
        act: function(params) {
            var self = this;
            params._token = token;
            $.post('games/' + self.currentGame.id, params, function(game){
                self.$set('game', game);
            }, 'json');
        }
    }
});